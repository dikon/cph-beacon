# BEACON-Datei des Catalogus Professorum Halensis

Der [Catalogus Professorum Halensis](https://www.catalogus-professorum-halensis.de/) wird von der Martin-Luther-Universität Halle-Wittenberg bereitgestellt und gibt biographische Auskunft über das professorale Lehrpersonal in der Zeit zwischen 1694 und 1968.

Die [hier](./data/gnd.txt) zu findende [BEACON-Datei](https://de.wikipedia.org/wiki/Wikipedia:BEACON), die die Professoren des Katalogs mit Identifikatoren der [Gemeinsamen Normdatei](https://www.dnb.de/gnd) (GND) verlinkt, wurde mithilfe des [BEACON Generators](https://wikidata-todo.toolforge.org/beacon.php) von [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page) erzeugt. Die dafür notwendige Zuordnung einzelner Einträge im Professorenkatalog zu Entitäten in Wikidata wurde [von Jonathan Groß initiiert](https://www.wikidata.org/wiki/Wikidata:Property_proposal/Archive/33#P2005) und via [Mix ’n’ Match](https://tools.wmflabs.org/mix-n-match/#/catalog/77) besorgt. Neben dem [Export der BEACON-Datei](http://tools.wmflabs.org/wikidata-todo/beacon.php?source=227&prop=2005) ermöglicht dies vor allem, Einträge des Professorenkatalogs in Wikidata mittels [Property](https://www.wikidata.org/wiki/Property:P2005) zu verlinken.

## Lizenz

„BEACON-Dateien sind grundsätzlich [Public Domain](./LICENSE).“ ([Quelle](https://de.wikipedia.org/wiki/Wikipedia:BEACON#Datenquellen_zu_Personen))
